//system components
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

//plugin components
import { TooltipModule } from 'ngx-bootstrap/tooltip';

//common components
import { routes } from './app.routes';
import { AppComponent } from './app.component';

//page components
import { WelcomeComponent } from './page/welcome/welcome.component';

//component components
import { HeaderComponent } from './component/header/header.component';
import { LeftbarComponent } from './component/leftbar/leftbar.component';

//service components

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LeftbarComponent,
    WelcomeComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes, {useHash: true}),
    TooltipModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
